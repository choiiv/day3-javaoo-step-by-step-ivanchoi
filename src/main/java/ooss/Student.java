package ooss;

public class Student extends Person {
    private Klass klass;
    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduction = super.introduce() + " I am a student.";
        if(klass != null) {
            if(klass.getLeader() != null && klass.getLeader().equals(this)) {
                introduction += String.format(" I am the leader of class %d.", klass.getNumber());
            }
            else {
                introduction += String.format(" I am in class %d.", klass.getNumber());
            }
        }
        return introduction;
    }

    public void join(Klass klass) {
        this.klass = klass;
        klass.attach(this);
    }

    @Override
    public void speak(Integer classNumber, String personName) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.", this.name, classNumber, personName);
    }

    public boolean isIn(Klass klass) {
        if(this.klass == null) {
            return false;
        }
        return this.klass.equals(klass);
    }

    public Klass getKlass() {
        return klass;
    }
}
