package ooss;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klasses = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String klassesString = klasses.stream().map(klass -> klass.getNumber().toString()).collect(Collectors.joining( ", "));
        String returnString = super.introduce() + " I am a teacher.";
        if(!klassesString.isEmpty()) {
            returnString += String.format(" I teach Class %s.", klassesString);
        }
        return returnString;
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
        klass.attach(this);
    }

    @Override
    public void speak(Integer classNumber, String personName) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.", this.name, classNumber, personName);
    }

    public boolean belongsTo(Klass klass2) {
        return this.klasses.stream().anyMatch(cur -> cur.equals(klass2));
    }

    public boolean isTeaching(Student student) {
        return this.belongsTo(student.getKlass());
    }
}
