package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private Integer number;
    private Student leader;
    private List<Person> persons = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Klass klass = (Klass) object;

        return Objects.equals(number, klass.number);
    }

    @Override
    public int hashCode() {
        return number != null ? number.hashCode() : 0;
    }

    public Integer getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if(!student.isIn(this)) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = student;
        this.persons.forEach((person) -> person.speak(number, student.getName()));
    }

    public boolean isLeader(Student student) {
        if(this.leader == null) {
            return false;
        }
        return this.leader.equals(student);
    }

    public Student getLeader() {
        return leader;
    }

    public void attach(Person person) {
        this.persons.add(person);
    }
}
